/* See LICENSE file for copyright and license details. */
/* Default settings; can be overriden by command line. */

static int topbar = 1;                     /* -b  option; if 0, dmenu appears at bottom              */
static int fuzzy = 0;                      /* -F  option; if 0, dmenu uses fuzzy matching            */
static int centered = 0;                   /* -c option; centers dmenu on screen                     */
static int restrict_return = 0;            /* -r option; if 1, disables shift-return and ctrl-return */
static int min_width = 800;                /* minimum width when centered                            */
static const char passchar = ' ';          /* password character */
/* -fn option overrides fonts[0]; default X11 font or font set */
static const char *fonts[] = {
	"Monospace:size=12:antialias=true",
	// "Ubuntu Mono:size=14:antialias=true",
	// "Sahel:size=12:antialias=true",
	// "UbuntuMono Nerd Font:size=12:antialias=true",
	// "Noto Color Emoji:size=12:antialias=true:autohint=true",
};
static const char *prompt = NULL;      /* -p  option; prompt to the left of input field         */

static const unsigned int baralpha = 0xff;
static const unsigned int borderalpha = OPAQUE;
static const unsigned int alphas[][3]      = {
	/*               fg      bg        border     */
	[SchemeNorm] = { OPAQUE, baralpha, borderalpha },
	[SchemeSel]  = { OPAQUE, baralpha, borderalpha },
	[SchemeOut]  = { OPAQUE, baralpha, borderalpha },
	[SchemeSelHighlight]  = { OPAQUE, baralpha, borderalpha },
	[SchemeNormHighlight] = { OPAQUE, baralpha, borderalpha },
};

#include "colors/void.h" /* default colorscheme */

/* -l and -g options; controls number of lines and columns in grid if > 0 */
static unsigned int lines      = 0;
static unsigned int columns    = 0;

/*
 * Characters not considered part of a word while deleting words
 * for example: " /?\"&[]"
 */
static const char worddelimiters[] = " ";

/* Size of the window border */
static unsigned int border_width = 0;
