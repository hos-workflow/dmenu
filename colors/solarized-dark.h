// #define ACCENTCOLOR "#a57705"
// #define ACCENTCOLOR "#728905"
#define ACCENTCOLOR "#2075c7"
// #define ACCENTCOLOR "#5856b9"
// #define ACCENTCOLOR "#c61b6e"
// #define ACCENTCOLOR "#e9e2cb"

static char normal_fg[]       = "#fdf6e3";
static char normal_bg[]       = "#001e26";
static char focus_fg[]        = "#001e26";
static char focus_bg[]        = ACCENTCOLOR;
static char sel_fg[]          = "#073642";
static char sel_bg[]          = "#93a1a1";
static char high_normal_fg[]  = "#000000";
static char high_normal_bg[]  = "#d01b24";
static char high_sel_fg[]     = "#fdf6e3";
static char high_sel_bg[]     = "#d01b24";

static const char *colors[SchemeLast][2] = {
	/*     fg         bg       */
	[SchemeNorm]          = { normal_fg,         normal_bg      },
	[SchemeSel]           = { focus_fg,          focus_bg       },
	[SchemeOut]           = { sel_fg,            sel_bg         },
	[SchemeNormHighlight] = { high_normal_fg,    high_normal_bg },
	[SchemeSelHighlight]  = { high_sel_fg,       high_sel_bg    },
};
