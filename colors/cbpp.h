static const char normal_fg[]      = "#bbbbbb";
static const char normal_bg[]      = "#232323";
static const char focus_fg[]       = "#eeeeee";
static const char focus_bg[]       = "#656565";
static const char sel_fg[]         = "#eeeeee";
static const char sel_bg[]         = "#323232";
static const char high_sel_fg[]    = "#323232";
static const char high_sel_bg[]    = "#eeeeee";
static const char high_normal_fg[] = "#323232";
static const char high_normal_bg[] = "#eeeeee";

static const char *colors[SchemeLast][2] = {
	/*     fg         bg       */
	[SchemeNorm]          = { normal_fg,         normal_bg      },
	[SchemeSel]           = { focus_fg,          focus_bg       },
	[SchemeOut]           = { sel_fg,            sel_bg         },
	[SchemeNormHighlight] = { high_normal_fg,    high_normal_bg },
	[SchemeSelHighlight]  = { high_sel_fg,       high_sel_bg    },
};
