static const char normal_fg[]      = "#eeeeec";
static const char normal_bg[]      = "#000000";
static const char focus_fg[]       = "#000000";
static const char focus_bg[]       = "#8bb158";
static const char sel_fg[]         = "#010101";
static const char sel_bg[]         = "#b74c4a";
static const char high_sel_fg[]    = "#cccccc";
static const char high_sel_bg[]    = "#b74c4a";
static const char high_normal_fg[] = "#010101";
static const char high_normal_bg[] = "#b74c4a";

static const char *colors[SchemeLast][2] = {
	/*     fg         bg       */
	[SchemeNorm]          = { normal_fg,         normal_bg      },
	[SchemeSel]           = { focus_fg,          focus_bg       },
	[SchemeOut]           = { sel_fg,            sel_bg         },
	[SchemeNormHighlight] = { high_normal_fg,    high_normal_bg },
	[SchemeSelHighlight]  = { high_sel_fg,       high_sel_bg    },
};
